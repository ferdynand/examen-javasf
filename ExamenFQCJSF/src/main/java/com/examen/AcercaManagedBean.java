package com.examen;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.examen.model.Imagen;

@SessionScoped
@ManagedBean(name="acercaManagedBean")
public class AcercaManagedBean {
	private List<Imagen> imagenes;
	
	public List<Imagen> getImagenes() {
		return imagenes;
	}

	public void setImagenes(List<Imagen> imagenes) {
		this.imagenes = imagenes;
	}

	public AcercaManagedBean() {
		imagenes = new ArrayList<Imagen>();
		imagenes.add(new Imagen("image1.png"));
		imagenes.add(new Imagen("image2.png"));
		imagenes.add(new Imagen("image3.png"));
	}

	public String Contactos() {
		return "contactos";
	}
}
