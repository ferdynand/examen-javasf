package com.examen;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.examen.model.Datos;


@SessionScoped
@ManagedBean(name="contactosManagedBean")
public class ContactosManagedBean {
	private Datos datos = new Datos("", "");
	private String email;
	private String mensaje;
	public String Acerca() {
		return "acerca";
	}
	public ContactosManagedBean() {
		super();
//		datos = new Datos("", "");
//		datos.setEmail("");
//		datos.setMensaje("");
	}
	public ContactosManagedBean(Datos datos, String email, String mensaje) {
		super();
		this.datos = datos;
		this.email = email;
		this.mensaje = mensaje;
	}
	public Datos getUsuario() {
		return datos;
	}
	public void setUsuario(Datos usuario) {
		this.datos = usuario;
	}

	public String Enviar() {
		if(this.mensaje.length()>50) {
			this.mensaje = this.mensaje.substring(0, 49);
		}
		return "confirmacion";
	}
	public Datos getDatos() {
		return datos;
	}
	public void setDatos(Datos datos) {
		this.datos = datos;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
