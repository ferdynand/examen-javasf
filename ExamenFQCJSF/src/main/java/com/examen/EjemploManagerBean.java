package com.examen;

import javax.faces.bean.ManagedBean;

@ManagedBean(name="ejemploManagerBean")
public class EjemploManagerBean {

	private String nombre;
	private int edad;
	private double estatura;
	private boolean casado;
	public EjemploManagerBean(){
		nombre = "N/A";
		edad = 18;
		estatura = 1.7;
		casado = false;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public double getEstatura() {
		return estatura;
	}
	public void setEstatura(double estatura) {
		this.estatura = estatura;
	}
	public boolean isCasado() {
		return casado;
	}
	public void setCasado(boolean casado) {
		this.casado = casado;
	}
	
}
