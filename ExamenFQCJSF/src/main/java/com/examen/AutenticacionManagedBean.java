package com.examen;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.examen.model.Usuario;

@SessionScoped
@ManagedBean(name="autenticacionManagedBean")
public class AutenticacionManagedBean {
	private Usuario usuario;
	private String error = "";

	public AutenticacionManagedBean() {
		super();
		usuario = new Usuario();
		usuario.setUsuario("");
		usuario.setPassword("");
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String Verifica() {
		System.out.println(usuario.getUsuario());
		System.out.println(usuario.getPassword());
		String admin = "admin";
		if(admin.equals(usuario.getUsuario()) && admin.equals(usuario.getPassword())) {
			return "acerca";
		}else {
			setError("error de credenciales");
			return "index";
		}
	}
}
