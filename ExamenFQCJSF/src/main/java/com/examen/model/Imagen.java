package com.examen.model;

public class Imagen {

	private String imagen;

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public Imagen() {
		super();
	}

	public Imagen(String imagen) {
		this.imagen = imagen;
	}
	
}
