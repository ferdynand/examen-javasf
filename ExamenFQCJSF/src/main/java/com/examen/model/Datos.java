package com.examen.model;

public class Datos {
	private String email;
	private String mensaje;
	public Datos() {
		super();
	}
	public Datos(String email, String mensaje) {
		super();
		this.email = email;
		this.mensaje = mensaje;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}
